/**
 * @file mindGapper.cpp
 * @brief Implementation of paper by Bohg, 2010: Mind the Gap: Robotic Grasping under Incomplete Observation
 * @author A. Huaman Quispe <ahuaman3@gatech.edu>
 * @date 2014/08/07
 */
#include <tabletop_symmetry/mindGapper.h>
#include <tabletop_symmetry/impl/mindGapper.hpp>
