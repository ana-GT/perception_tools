#include <crichton_nodes/crichton_eye.h>
#include <crichton_nodes/tabletop_segmentation/tabletop_segmentation.h>
#include <tf/transform_datatypes.h>

/**
 * @brief Constructor
 */
Crichton_Eye::Crichton_Eye( ros::NodeHandle &_nh ) {
nh_ = _nh;
ros_setup();

  depth_cloud_.reset( new pcl::PointCloud<pcl::PointXYZ>() );
  width_ = 640;
  height_ = 480;
  f_ = 525.0;
  filter_x_[0] = -0.5; filter_x_[1] = 0.5;
  filter_y_[0] = -0.5; filter_y_[1] = 0.5;
  filter_z_[0] = 0.2; filter_z_[1] = 0.75;
reference_frame_ = "world";
}

/**
 * @function ros_setup
 */
void Crichton_Eye::ros_setup() {
  depth_img_sub_ = nh_.subscribe("/camera/depth_registered/image_raw", 1,
				 &Crichton_Eye::depth_cb,
				 this );
  cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>( ros::this_node::getName() + "/pointcloud", 1 );
  fitting_srv_ = nh_.advertiseService( ros::this_node::getName() + "/fitting_primitives",
				      &Crichton_Eye::fitting_srv, this);

primitives_pub_ = nh_.advertise<visualization_msgs::MarkerArray>( ros::this_node::getName() + "/primitive_objects", 1 );

}

/**
 * @function fitting_srv
 */
bool Crichton_Eye::fitting_srv( std_srvs::Empty::Request& req,
				std_srvs::Empty::Response& resp) {
  printf("** Fitting srv **\n");
  depth_mutex_.lock();
  // 1. Tabletop fitting
  TabletopSegmentor<pcl::PointXYZ> tts;
  tts.set_filter_minMax( -0.75, 0.75, -0.75, 0.75, 0.25, 0.9 );
  tts.processCloud( depth_cloud_ );
  //gTableCoeffs = tts.getTableCoeffs();
  
  depth_mutex_.unlock();
  
  table_points_ = tts.getTable();
  // 2. Euclideant clusters
  int n = tts.getNumClusters();
  printf("Num of clusters: %d \n", n );
  
  // 3. SQ Fitting
  visualization_msgs::Marker marker;
  marker.action = marker.DELETEALL;
  marker_primitives_.markers.push_back(marker);
  primitives_pub_.publish( marker_primitives_ );
  
  usleep(0.05*1e6);
  ros::spinOnce();
  marker_primitives_.markers.resize(0);
  
  SQ_fitter<pcl::PointXYZ> fitter;
  for( int i = 0; i < n; ++i ) {
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr cluster( new pcl::PointCloud<pcl::PointXYZ>() );
    *cluster = tts.getCluster(i);
  
    fitter.setInputCloud( cluster );
    double smax = 0.03; double smin = 0.005; int N = 5; double thresh = 0.005;
    fitter.fit( SQ_FX_RADIAL,  smax, smin, N, thresh );
      
      SQ_parameters p;
      fitter.getFinalParams( p );	    

      printf("SQ [%d]: Dim: %f %f %f -- e: %f %f . Trans: %f %f %f. Rot: %f %f %f\n",
	     i, p.dim[0], p.dim[1], p.dim[2],
	     p.e[0], p.e[1],
	     p.trans[0], p.trans[1], p.trans[2],
	     p.rot[0], p.rot[1], p.rot[2] );
      visualization_msgs::Marker msg;
      fill_marker( msg, p, std::to_string(i) );
      marker_primitives_.markers.push_back( msg );
      
      
  } // end for

  // Brief Publishing results
  primitives_pub_.publish( marker_primitives_ );

}

/**
 * @function fill_marker
 */
void Crichton_Eye::fill_marker( visualization_msgs::Marker &_marker,
				SQ_parameters _p, std::string ns ) {

_marker.header.stamp = ros::Time(0);//::now();
  _marker.header.frame_id = reference_frame_;
  _marker.ns = ns;
if( _p.e[0] < 0.3 && _p.e[1] < 0.3 ) {
_marker.type = visualization_msgs::Marker::CUBE;
} else if(  _p.e[0] > 0.75 && _p.e[1] < 0.3 ) {
  _marker.type = visualization_msgs::Marker::CUBE; // CYLINDER
} else {
  _marker.type = visualization_msgs::Marker::CUBE; // SPHERE, I FORGOT THOSE NUMBERS
}

  _marker.action = _marker.ADD; // Add/Modify

_marker.pose.position.x = _p.trans[0];
_marker.pose.position.y = _p.trans[1];
_marker.pose.position.z = _p.trans[2];
tf::Quaternion qi; qi.setRPY( _p.rot[0], _p.rot[1], _p.rot[2] );
_marker.pose.orientation.x = qi.getX();
_marker.pose.orientation.y = qi.getY();
_marker.pose.orientation.z = qi.getZ();
_marker.pose.orientation.w = qi.getW();

if( _marker.type == visualization_msgs::Marker::CUBE ) {
  _marker.scale.x = 2.0*_p.dim[0];
  _marker.scale.y = 2.0*_p.dim[1];
  _marker.scale.z = 2.0*_p.dim[2];
} if( _marker.type == visualization_msgs::Marker::SPHERE ) {
    _marker.scale.x = 2.0*_p.dim[0]; // DIAMETER	
    _marker.scale.y = 2.0*_p.dim[1];
    _marker.scale.z = 2.0*_p.dim[2];
  } else if( _marker.type == visualization_msgs::Marker::CYLINDER ) {
    _marker.scale.x = 2.0*_p.dim[0]; // DIAMETER
    _marker.scale.y = 2.0*_p.dim[1]; // DIAMETER
    _marker.scale.z = 2.0*_p.dim[2]; // HEIGHT
  }
  _marker.color.r = 0.0; _marker.color.g = 1.0;
  _marker.color.b = 0.0; _marker.color.a = 1.0;
  _marker.lifetime = ros::Duration(0); // 0: Forever
  _marker.frame_locked = true;

}

/**
 * @function depth_Db
 */
void Crichton_Eye::depth_cb( const sensor_msgs::ImageConstPtr &_msg) {

  depth_mutex_.lock();

  cv_bridge::CvImagePtr cv_ptr;
  
  try {
    cv_ptr = cv_bridge::toCvCopy( _msg );
  } catch( cv_bridge::Exception &_e ) {
    ROS_ERROR("cv_bridge exception: %s", _e.what() );
    depth_mutex_.unlock();
    return;
  }

  // Mat output in this case is
  cv::Mat img = cv_ptr->image;
  if( img.type() != CV_32FC1 ) {
    printf("Image in OpenCV is NOT of type 32FC1 but of type: %d \n", img.type() );
    depth_mutex_.unlock();
    return;
  }
 
  // Reset
  depth_cloud_->points.resize(0);
  
  // Check limits for values in matrix
  float zi_max = -1000.0; 
  float zi_min = 1000.0;
  float xi, yi, zi;
  
  for( unsigned int v = 0; v < img.rows; ++v ) {
    for( unsigned u = 0; u < img.cols; ++u ) {
      zi = img.at<float>( v, u );
      xi = ( (float)u - 0.5*width_ )*zi/f_;
      yi = ( (float)v - 0.5*height_ )*zi/f_;
      
      if( zi > zi_max ) { zi_max = zi; }
      if( zi < zi_min ) { zi_min = zi; }

      // If out of bounds, stick it to 0
      if( xi < filter_x_[0] || 
	  xi > filter_x_[1] ||
	  yi < filter_y_[0] ||
	  yi > filter_y_[1] ||
	  zi < filter_z_[0] ||
	  zi > filter_z_[1] ) {
	xi = 0; yi = 0; zi = 0;
      }

      // Store it
      pcl::PointXYZ p( xi, yi, zi );
      depth_cloud_->points.push_back( p );

    }
  }
  depth_cloud_->width = width_;
  depth_cloud_->height = height_;
    
  //printf("\t ** Extreme values min-max: %f %f \n", zi_min, zi_max );

  sensor_msgs::PointCloud2 depth_msg;
  pcl::toROSMsg( *depth_cloud_, depth_msg );
  depth_msg.header.frame_id = reference_frame_;

  cloud_pub_.publish( depth_msg );

  depth_mutex_.unlock();
 
}


//////////////////////////////////
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "crichton_eye" );
  ros::NodeHandle nh;
  Crichton_Eye ce( nh );

  ros::Rate r(30.0);
  while( ros::ok() ) {
    r.sleep();
    ros::spinOnce();
  } // while

}
