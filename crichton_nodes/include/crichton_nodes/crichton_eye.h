#pragma once

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

#include<std_srvs/Empty.h>
#include <mutex>

#include <sq_tools/SQ_parameters.h>
#include <sq_tools/SQ_fitter.h>


/**
 * @class Crichton_Eye
 */
class Crichton_Eye {

 public:
  Crichton_Eye( ros::NodeHandle &_nh );
  void ros_setup();
  void depth_cb( const sensor_msgs::ImageConstPtr &_msg);
  bool fitting_srv( std_srvs::Empty::Request& req,
		    std_srvs::Empty::Response& resp );

  void fill_marker( visualization_msgs::Marker &_marker,
		    SQ_parameters _p, std::string ns );
  
 protected:
  ros::NodeHandle nh_;
  ros::Publisher cloud_pub_;
  ros::Subscriber depth_img_sub_;
  ros::ServiceServer fitting_srv_;
  
  int width_;
  int height_;
  double f_;

  pcl::PointCloud<pcl::PointXYZ>::Ptr depth_cloud_;
  pcl::PointCloud<pcl::PointXYZ> table_points_;
  std::mutex depth_mutex_;
  double filter_x_[2];
  double filter_y_[2];
  double filter_z_[2];

  ros::Publisher primitives_pub_;
  visualization_msgs::MarkerArray marker_primitives_;
  std::string reference_frame_;
};
